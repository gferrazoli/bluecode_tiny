defmodule BluecodeTinyWeb.TinyController do
  use BluecodeTinyWeb, :controller

  require Logger

  alias BluecodeTiny.Tiny
  alias BluecodeTiny.Urls

  def encode_url(conn, %{"url" => base_url}) do
    tinyfy(base_url)
    |> render_result(base_url, conn)
  end

  def decode_tiny(conn, %{"tiny_url" => tiny_url}) do
    tiny_url
    |> get_by_tiny_url()
    |> render_result(tiny_url, conn)
  end

  defp render_result(nil, not_a_url, conn) do
    conn
    |> send_resp(
      400,
      Jason.encode!(%{
        error: "Not a valid url or the url doesn't exist in our databases",
        base_url: not_a_url
      })
    )
  end

  defp render_result({:error, error}, base_url, conn) do
    Logger.error("Error when creating tiny url from url: #{base_url}\nWith stack: #{error}")

    conn
    |> send_resp(
      500,
      Jason.encode!(%{
        error: "An error ocurred when creating tiny url from url",
        base_url: base_url
      })
    )
  end

  defp render_result({:ok, created_url}, base_url, conn) do
    conn
    |> send_resp(
      200,
      Jason.encode!(%{tiny_url: created_url.tiny_url, base_url: base_url})
    )
  end

  defp tinyfy(base_url) when is_bitstring(base_url) do
    base_url
    |> check_if_already_created()
    |> create_tiny_if_not_created(base_url)
  end

  defp tinyfy(_base_url), do: {:error, "Not a valid url"}

  defp check_if_already_created(base_url) do
    Urls.get_from_base_url(base_url)
  end

  defp get_by_tiny_url(tiny_url) when is_bitstring(tiny_url) do
    Urls.get_from_tiny_url(tiny_url)
  end

  defp get_by_tiny_url(_tiny_url), do: nil

  defp create_tiny_if_not_created(nil, base_url), do: Tiny.tinyfy_url(base_url)

  defp create_tiny_if_not_created(tiny_url_struct, _base_url), do: tiny_url_struct
end
