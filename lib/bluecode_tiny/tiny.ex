defmodule BluecodeTiny.Tiny do
  @moduledoc """
  This module is responsible for calling all the functions that make the
  url tiny and calling the method to save on the database after tinyfying
  """

  alias BluecodeTiny.Crypt.Encoder
  alias BluecodeTiny.Urls

  def tinyfy_url(base_url) do
    tiny_url =
      get_table_size()
      |> Encoder.encodeUrl()

    Urls.create_url(%{base_url: base_url, tiny_url: tiny_url})
  end

  defp get_table_size do
    table_size = Urls.get_url_table_size()

    if table_size == 0 do
      1
    else
      table_size
    end
  end
end
