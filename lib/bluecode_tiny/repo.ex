defmodule BluecodeTiny.Repo do
  use Ecto.Repo,
    otp_app: :bluecode_tiny,
    adapter: Ecto.Adapters.Postgres
end
