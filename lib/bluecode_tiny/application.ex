defmodule BluecodeTiny.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      BluecodeTinyWeb.Telemetry,
      # Start the Ecto repository
      BluecodeTiny.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: BluecodeTiny.PubSub},
      # Start Finch
      {Finch, name: BluecodeTiny.Finch},
      # Start the Endpoint (http/https)
      BluecodeTinyWeb.Endpoint
      # Start a worker by calling: BluecodeTiny.Worker.start_link(arg)
      # {BluecodeTiny.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BluecodeTiny.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    BluecodeTinyWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
