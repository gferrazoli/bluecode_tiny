defmodule BluecodeTiny.Crypt.Encoder do
  @alphabet ~w(
    a b c d e f g h i j k l m n o p q r s t u v w
    x y z A B C D E F G H I J K L M N O P Q R S T
    U V W X Y Z 0 1 2 3 4 5 6 7 8 9
    )

  def encodeUrl(id_to_encode) when id_to_encode == 0, do: Enum.at(@alphabet, 0)

  def encodeUrl(id_to_encode) do
    @alphabet
    |> length()
    |> encode("", id_to_encode)
    |> String.reverse()
  end

  defp encode(_base, string, id_to_encode) when id_to_encode <= 0, do: string

  defp encode(base, string, id_to_encode) do
    new_string = string <> Enum.at(@alphabet, rem(id_to_encode, base))

    encode(base, new_string, id_to_encode / base)
  end
end
