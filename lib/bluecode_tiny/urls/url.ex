defmodule BluecodeTiny.Urls.Url do
  @moduledoc """
  This module is reponsible for creating the struct for the url
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "urls" do
    field(:base_url, :string)
    field(:tiny_url, :string)

    timestamps()
  end

  @doc false
  def changeset(url, attrs) do
    url
    |> cast(attrs, [:tiny_url, :base_url])
    |> validate_required([:tiny_url, :base_url])
  end
end
