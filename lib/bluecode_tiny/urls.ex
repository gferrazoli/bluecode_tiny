defmodule BluecodeTiny.Urls do
  @moduledoc """
  The Urls context.
  """

  import Ecto.Query, warn: false
  alias BluecodeTiny.Repo

  alias BluecodeTiny.Urls.Url

  @doc """
  Returns the list of urls.

  ## Examples

      iex> list_urls()
      [%Url{}, ...]

  """
  def list_urls do
    Repo.all(Url)
  end

  @doc """
  Gets urls table size

  ## Examples

      iex> get_url_table_size()
      12

  """
  def get_url_table_size do
    list_urls()
    |> length()
  end

  @doc """
  Gets a single url from base url.

  Returns nil if Url doesn't exist

  ## Examples

      iex> get_url(https://www.google.com/)
      %Url{}

      iex> get_url(456)
      nil

  """
  def get_from_base_url(base_url), do: Repo.get_by(Url, base_url: base_url)

  @doc """
  Gets a single url from tiny url.

  Returns nil if Url doesn't exist

  ## Examples

      iex> get_url(localhost:300/dsat3)
      %Url{}

      iex> get_url(456)
      nil

  """
  def get_from_tiny_url(tiny_url), do: Repo.get_by(Url, tiny_url: tiny_url)

  @doc """
  Creates a url.

  ## Examples

      iex> create_url(%{field: value})
      {:ok, %Url{}}

      iex> create_url(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_url(attrs \\ %{}) do
    %Url{}
    |> Url.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking url changes.

  ## Examples

      iex> change_url(url)
      %Ecto.Changeset{data: %Url{}}

  """
  def change_url(%Url{} = url, attrs \\ %{}) do
    Url.changeset(url, attrs)
  end
end
