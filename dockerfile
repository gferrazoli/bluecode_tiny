FROM bitwalker/alpine-elixir-phoenix:latest

WORKDIR /app

COPY mix.exs .
COPY mix.lock .

EXPOSE 4000

RUN apk add --no-cache make gcc libc-dev

CMD mix deps.get && mix ecto.create && mix ecto.migrate && mix phx.server