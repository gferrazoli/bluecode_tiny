# BluecodeTiny

To start your Phoenix server:

- Run `docker compose up --build` to start your docker
- Server should now be up at localhost:4000/dev/dashboard

To access the endpoints:

- post at "/", to tinyfy a url
- get at "/:tiny_url", to decode a url

System does not work properly with docker due to a connection problem between the database and the application.
