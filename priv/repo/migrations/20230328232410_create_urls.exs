defmodule BluecodeTiny.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table(:urls) do
      add :tiny_url, :string
      add :base_url, :string

      timestamps()
    end
  end
end
