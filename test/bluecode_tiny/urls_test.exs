defmodule BluecodeTiny.UrlsTest do
  use BluecodeTiny.DataCase

  alias BluecodeTiny.Urls

  describe "urls" do
    alias BluecodeTiny.Urls.Url

    import BluecodeTiny.UrlsFixtures

    @invalid_attrs %{base_url: nil, tiny_url: nil}

    test "list_urls/0 returns all urls" do
      url = url_fixture()
      assert Urls.list_urls() == [url]
    end

    test "get_url!/1 returns the url with given id" do
      url = url_fixture()
      assert Urls.get_url!(url.id) == url
    end

    test "create_url/1 with valid data creates a url" do
      valid_attrs = %{base_url: "some base_url", tiny_url: "some tiny_url"}

      assert {:ok, %Url{} = url} = Urls.create_url(valid_attrs)
      assert url.base_url == "some base_url"
      assert url.tiny_url == "some tiny_url"
    end

    test "create_url/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Urls.create_url(@invalid_attrs)
    end

    test "update_url/2 with valid data updates the url" do
      url = url_fixture()
      update_attrs = %{base_url: "some updated base_url", tiny_url: "some updated tiny_url"}

      assert {:ok, %Url{} = url} = Urls.update_url(url, update_attrs)
      assert url.base_url == "some updated base_url"
      assert url.tiny_url == "some updated tiny_url"
    end

    test "update_url/2 with invalid data returns error changeset" do
      url = url_fixture()
      assert {:error, %Ecto.Changeset{}} = Urls.update_url(url, @invalid_attrs)
      assert url == Urls.get_url!(url.id)
    end

    test "delete_url/1 deletes the url" do
      url = url_fixture()
      assert {:ok, %Url{}} = Urls.delete_url(url)
      assert_raise Ecto.NoResultsError, fn -> Urls.get_url!(url.id) end
    end

    test "change_url/1 returns a url changeset" do
      url = url_fixture()
      assert %Ecto.Changeset{} = Urls.change_url(url)
    end
  end
end
