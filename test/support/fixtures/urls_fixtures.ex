defmodule BluecodeTiny.UrlsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BluecodeTiny.Urls` context.
  """

  @doc """
  Generate a url.
  """
  def url_fixture(attrs \\ %{}) do
    {:ok, url} =
      attrs
      |> Enum.into(%{
        base_url: "some base_url",
        tiny_url: "some tiny_url"
      })
      |> BluecodeTiny.Urls.create_url()

    url
  end
end
